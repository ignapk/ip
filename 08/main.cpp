#include "A.h"
#include "B.h"
#include "C.h"

int main()
{
	A a;
	B b;
	C c;
	a.setB(&b);
	b.setC(&c);
	c.setB(&b);
	a.fooA();
}
