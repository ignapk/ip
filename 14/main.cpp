#include "X86Factory.h"
#include "ArmFactory.h"
#include "PC.h"
#include "Laptop.h"

int main ()
{
	X86Factory *x86Factory = new X86Factory();
	ArmFactory *armFactory = new ArmFactory();
	PC *x86PC = x86Factory->CreatePC();
	PC *armPC = armFactory->CreatePC();
	Laptop *x86Laptop = x86Factory->CreateLaptop();
	Laptop *armLaptop = armFactory->CreateLaptop();
	x86PC->display();
	armPC->display();
	x86Laptop->display();
	armLaptop->display();
	return 0;
}
