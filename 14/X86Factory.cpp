//
//
//  Generated by StarUML(tm) C++ Add-In
//
//  @ Project : Untitled
//  @ File Name : X86Factory.cpp
//  @ Date : 6/21/2023
//  @ Author : 
//
//


#include "X86Factory.h"
#include "X86PC.h"
#include "X86Laptop.h"

PC * X86Factory::CreatePC() {
	return new X86PC();
}

Laptop * X86Factory::CreateLaptop() {
	return new X86Laptop();
}

