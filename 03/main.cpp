#include "Kalkulator.h"
#include "Student.h"
#include "NumerWieszaka.h"
#include "Kolo.h"

int main ()
{
	int a = 1, b = 2;
	Student s;
	Kalkulator k;
	cout << s.dodaj(a, b, &k) << endl;
	Smiec *smiec = s.utworzSmiec();
	cout << "Student stworzyl smiec rodzaju: " << smiec->rodzaj << endl;
	NumerWieszaka numerWieszaka(2, &s);
	s.ustawNumerWieszaka(&numerWieszaka);
	cout << "Student ma numer wieszaka: " << s.dajNumerWieszaka()->dajNumerek() << endl;
	cout << "Tworze kolo" << endl;
	Kolo *kolo = new Kolo("SKNI");
	kolo->ustawCzlonka("Ignacy");
	kolo->wypiszCzlonka();
	kolo->ustawStatut("Statut SKNI");
	kolo->wypiszStatut();
	cout << "Usuwam kolo" << endl;
	delete kolo;
}
