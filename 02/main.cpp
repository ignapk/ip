#include "Brytyjski.h"
#include "Perski.h"
#include "Jamnik.h"

int main ()
{
	Brytyjski brytyjski("samiec", false, "Sekretny identyfikator 007");
	Perski perski("samica", true, "Mistyczny pierscien");
	Jamnik jamnik("samica", false, "Bardzo dlugi ogon");

	cout << "Kot brytyjski: " << endl << "Moja plec to: " << brytyjski.dajPlec() << endl << "Czy mam wasy: " << brytyjski.czyWasy() << endl << "Mam: " << brytyjski.dajID() << endl;
	brytyjski.oddycha();
	brytyjski.mrucz();

	cout << "Kot perski: " << endl << "Moja plec to: " << perski.dajPlec() << endl << "Czy mam wasy: " << perski.czyWasy() << endl << "Mam: " << perski.dajPierscien() << endl;
	perski.oddycha();
	perski.mrucz();

	cout << "Pies jamnik: " << endl << "Moja plec to: " << jamnik.dajPlec() << endl << "Czy mam kaganiec: " << jamnik.czyKaganiec() << endl << "Mam: " << jamnik.dajOgon() << endl;
	jamnik.oddycha();
	jamnik.szczekaj();

	return 0;
}
