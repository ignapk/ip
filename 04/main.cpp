#include "Kalkulator.h"
#include "Student.h"
#include "NumerWieszaka.h"
#include "Kolo.h"

int main ()
{
	int a = 1, b = 2;
	Student s;
	s.ustawImie("Ignacy");
	Kalkulator k;
	cout << s.dodaj(a, b, &k) << endl;
	Smiec *smiec = s.utworzSmiec();
	cout << "Student stworzyl smiec rodzaju: " << smiec->rodzaj << endl;
	NumerWieszaka numerWieszaka(2, &s);
	s.ustawNumerWieszaka(&numerWieszaka);
	cout << "Student ma numer wieszaka: " << s.dajNumerWieszaka()->dajNumerek() << endl;
	cout << "Tworze kolo" << endl;
	Kolo *kolo = new Kolo("SKNI");
	Student s2;
	s2.ustawImie("Krystian");
	numerWieszaka.ustawStudenta(&s2);
	cout << "Wieszak o numerze: " << numerWieszaka.dajNumerek() << " jest przyporzadkowany studentom: " << numerWieszaka.dajStudenta()->dajImie() << " oraz " << numerWieszaka.dajStudenta()->dajImie() << endl;
	kolo->dodajCzlonka(&s);
	kolo->dodajCzlonka(&s2);
	kolo->ustawStatut("Statut SKNI");
	kolo->wypiszStatut();
	kolo->usunCzlonka();
	kolo->usunCzlonka();
	cout << "Usuwam kolo" << endl;
	delete kolo;
}
