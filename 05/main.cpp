#include "Sklep.h"

int main ()
{
	Piwo *somersby = new Piwo();
	somersby->ustawNazwe("Somersby");
	somersby->ustawCene(5.39);
	somersby->ustawProcent(4.5);

	Piwo *garage = new Piwo();
	garage->ustawNazwe("Garage");
	garage->ustawCene(5.80);
	garage->ustawProcent(4.6);

	Sklep *sklep = new Sklep();
	sklep->dodajPiwo(somersby);
	sklep->dodajPiwo(garage);
	
	sklep->pokazPiwo(0);
	sklep->pokazPiwo(1);

	sklep->usunPiwo();
	return 0;
}
