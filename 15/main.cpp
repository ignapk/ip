#include "BinarySearch.h"
#include "LinearSearch.h"

int main ()
{
	int arr[10] = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34};
	int *ret;

	BinarySearch *binary = new BinarySearch();
	ret = binary->search(arr, 5, 10);
	if (ret == nullptr)
		std::cout << "No such value.\n";
	else
		std::cout << "Found at position " << ret - arr << ".\n";

	LinearSearch *linear = new LinearSearch();
	ret = linear->search(arr, 5, 10);
	if (ret == nullptr)
		std::cout << "No such value.\n";
	else
		std::cout << "Found at position " << ret - arr << ".\n";

	delete binary;
	delete linear;
	
	return 0;
}
